#!/bin/bash


if [ ! -x /var/lib/elasticsearch ]; then

        echo "INSTALLING elasticsearch"
echo    
echo

         
	echo "Y" | apt install wget
echo
echo
	echo "Y" | apt-get install apt-transport-https
echo
echo
	 echo "Y" | apt install openjdk-8-jdk
echo
echo
	 wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
	
	 echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
echo 
echo 
	echo "Y" | apt-get install elasticsearch
	
	sed -i '2d' /etc/elasticsearch/elasticsearch.yml
	sed -i '55inetwork.host: localhost' /etc/elasticsearch/elasticsearch.yml
	systemctl enable elasticsearch
	systemctl start elasticsearch
echo
echo
	systemctl status elasticsearch
echo
echo
        echo "SUCESSFULLY INSTALLED elasticsearch"

else
echo
echo
        echo "ALREADY INSTALLED elasticsearch"
echo
echo
fi
