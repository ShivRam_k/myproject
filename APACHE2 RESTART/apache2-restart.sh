#!/bin/bash
ACTIVE=$(/etc/init.d/apache2 status | awk '{print $2}' | grep -i inactive)

if [ "$ACTIVE" = inactive ]
then    echo "apache2 is inactive"  
	systemctl restart apache2
	echo "apache2 restarted"
else
	echo "apache running"
fi
