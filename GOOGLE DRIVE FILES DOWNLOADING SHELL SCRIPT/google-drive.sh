#!/bin/bash

  #creates the directory to store the downloaded file
    
       mkdir  -p /tmp/gdrive-files

  #installing curl
     
      apt-get install curl -y

  #reads the file Id
     
      echo -n "Provide the fileId of gdrive link (eg:1mAbq7iR7wYzGFNQRwiwpIA0tQ):"
      read fileId;

  #reads the file Name
  
     echo -n "Provide the fileName of gdrive file to download:"
     read fileName;

  #changes the directory to download.
 
    cd /tmp/gdrive-files
   
  #Dowloading process

    curl -sc /tmp/cookie "https://drive.google.com/uc?export=download&id=${fileId}" > /dev/null
    source="$(awk '/_warning_/ {print $NF}' /tmp/cookie)"
    curl -Lb /tmp/cookie "https://drive.google.com/uc?export=download&confirm=${source}&id=${fileId}" -o ${fileName}

  #removes the cookie file 

    rm -rf /tmp/cookie

    echo "files successfully stored in /tmp/gdrive-files/$fileName"
