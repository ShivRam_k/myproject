#!/bin/bash
ACTIVE=$(/etc/init.d/mysql status | awk '{print $2}' | grep -i inactive)

if [ "$ACTIVE" = inactive ]
then    echo "mysql is inactive"  
	systemctl restart mysql
	echo "mysql restarted"
else
	echo "mysql running"
fi

